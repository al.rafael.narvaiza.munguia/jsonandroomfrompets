package com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class petList {
    @SerializedName("pet")
    @Expose
    private List<Pet> pet = null;
    public petList(){

    }
    public petList(List<Pet> pet){
        super ();
        this.pet = pet;
    }
    public List<Pet> getMascota() {
        return pet;
    }

    public void setMascota(List<Pet> pet) {
        this.pet = pet;
    }

    public petList withResult(List<Pet> petLists){
        this.pet =petLists;
        return this;
    }
}
