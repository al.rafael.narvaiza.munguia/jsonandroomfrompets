package com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.network;

import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.retrofit.petList;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {

    // uso con servidor web local
    @GET("files/pets.json")
    Call<petList> getPetList();
}
