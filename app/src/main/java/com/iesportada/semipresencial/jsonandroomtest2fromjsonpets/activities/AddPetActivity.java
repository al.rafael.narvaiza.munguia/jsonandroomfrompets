package com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.R;
import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.databinding.ActivityAddPetBinding;
import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.db.model.MyPet;
import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.sandbox.Utils;
import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.viewmodel.MainActivityViewModel;


public class AddPetActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{


    private ActivityAddPetBinding binding;
    private MainActivityViewModel viewModelInsert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pet);
        binding = ActivityAddPetBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        viewModelInsert = new ViewModelProvider(this).get(MainActivityViewModel.class);

        binding.buttonDiscard.setOnClickListener(v->finish());
        binding.buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre, raza, imagen, edad;
                MyPet myPet;
                nombre = binding.editTextTextPerroName.getText().toString();
                raza = binding.editTextTextPerroRaza.getText().toString();
                imagen = binding.editTextTextPerroImagen.getText().toString();
                edad = binding.editTextNumber.getText().toString();
                if(!Utils.inputFieldsAreChecked(nombre, raza, imagen, edad).isEmpty()){

                    showMessage(Utils.inputFieldsAreChecked(nombre, raza, imagen, edad));
                }
                else {
                    myPet=new MyPet(nombre,raza,edad,imagen);
                    viewModelInsert.insert(myPet);
                    //viewModelInsert.insertBooking(nombre, imagen, raza, edad);
                    finish();
                }
            }
        });
        viewModelInsert.getPets();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        parent.getItemAtPosition(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}