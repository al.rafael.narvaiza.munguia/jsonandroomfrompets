package com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.databinding.ActivityMainBinding;
import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.db.model.MyPet;
import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.network.ApiAdapter;
import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.recyclerview.PetListAdapter;
import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.retrofit.Pet;
import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.viewmodel.MainActivityViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity  implements PetListAdapter.HandlePetClick{

    private MainActivityViewModel viewModel;
    ActivityMainBinding binding;
    PetListAdapter petListAdapter;
    ArrayList<Pet> petLists;
    List<MyPet> myPetList;
    ProgressDialog progres;
    public static Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        context=getApplicationContext();
        getPets();
        initViewModel();
        updatePets();
        initRecyclerView();
        binding.floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddPetActivity.class);
                startActivity(intent);
            }
        });
    }

    public List<MyPet> getPetList(){
        return myPetList;
    }

    public void initViewModel(){
        viewModel = new ViewModelProvider(this).get(MainActivityViewModel.class);
        viewModel.getPets().observe(this, new Observer<List<MyPet>>() {
            @Override
            public void onChanged(List<MyPet> myPets) {
                petListAdapter.setPetList(myPets);
                setPetList(myPets);
            }
        });
    }

    private void initRecyclerView(){
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        petListAdapter = new PetListAdapter(this, this);
        binding.recyclerView.setAdapter(petListAdapter);

    }

    private void setPetList(List<MyPet> myPets) {
        this.myPetList = myPets;
    }

    private void getPets() {
        Context context;
        context=this;
        progres = new ProgressDialog(context);
        progres.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progres.setMessage("Descargando mascotas, espere");
        progres.setCancelable(true);
        progres.show();
        Call<com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.retrofit.petList> petListCall = ApiAdapter.getInstance().getPetList();
        petListCall.enqueue(new Callback<com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.retrofit.petList>() {
            @Override
            public void onResponse(Call<com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.retrofit.petList> call, Response<com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.retrofit.petList> response) {
                if(response.isSuccessful()){
                    List<com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.retrofit.petList> pets = Collections.singletonList(response.body());

                    petLists=new ArrayList<>();
                    for(com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.retrofit.petList pl: pets){

                        petLists=(ArrayList<Pet>) pl.getMascota();
                        for(Pet m : petLists){
                            MyPet myPet = new MyPet();
                            myPet.setName(m.getName());
                            myPet.setRace(m.getRace());
                            myPet.setAge(String.valueOf(m.getAge()));
                            myPet.setPhoto("https://dam.org.es/" + m.getImage());
                            List<MyPet> savedMyPets = getPetList();
                            if(savedMyPets.size()<=1){
                                viewModel.insert(myPet);
                            }
                            else{
                                if(!isItAdded(myPet)){
                                    viewModel.insert(myPet);
                                }
                            }
                        }
                    }
                }
                progres.dismiss();
            }


            @Override
            public void onFailure(Call<com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.retrofit.petList> call, Throwable t) {
                System.out.println(t.getMessage());
                progres.dismiss();
            }
        });

    }

    public boolean isItAdded(MyPet myPet){
        boolean exist = false;
        List<MyPet> myPets = getPetList();
        for( MyPet p : myPets){
            if(p.getName().equalsIgnoreCase(myPet.getName())&&p.getAge().equalsIgnoreCase(myPet.getAge())){
                exist = true;
            }
        }
        return exist;
    }

    private void showMessage(String s){
        Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPostResume(){
        super.onPostResume();
        viewModel.getPets();
        updatePets();
    }

    private void updatePets() {

        if(petListAdapter==null) return;
        List<MyPet> myPets = getPetList();
        petListAdapter.setPetList(myPets);
        petListAdapter.notifyDataSetChanged();
    }


    @Override
    public void editPet(MyPet myPet) {

    }

    @Override
    public void deletePet(MyPet myPet) {

    }
}