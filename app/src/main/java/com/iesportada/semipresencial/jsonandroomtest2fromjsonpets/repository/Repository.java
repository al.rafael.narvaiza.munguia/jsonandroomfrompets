package com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.db.PetDao;
import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.db.PetDatabase;
import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.db.model.MyPet;

import java.util.Arrays;
import java.util.List;

public class Repository {
    private final LiveData<List<MyPet>> petList;
    private final PetDao petListDao;

    public Repository(Application application){
        PetDatabase petDatabase = PetDatabase.getInstance(application);
        petListDao = petDatabase.petListDao();
        petList = petListDao.getAllPets();
    }

    public LiveData<List<MyPet>> getPetList() {
        return petList;
    }

    public LiveData<MyPet> getPet(int id){
        return petListDao.getUniquePet(id);
    }


    public void addPet(MyPet myPet){
       new insertAsync(petListDao).execute(myPet);
    }
    public void deletePet(MyPet myPet){
        new deleteAsync(petListDao).execute(myPet);
    }
    public void updatePet(MyPet myPet){
        new updateAsync(petListDao).execute(myPet);
    }


    /**
     * Al habilitar la ejecución en el UI de las consultas a room si nos permite hacer esto.
     */
    /*
    public void updateSelectedPet(String nombre, String raza, String imagen, String edad, int id){
        PetDao petDao = petListDao;
        petDao.updateSelectedPet(nombre,raza,imagen,edad, id);
    }

     */

    private static class insertAsync extends AsyncTask<MyPet, Void, Void> {
        PetDao petDao;
        public insertAsync(PetDao petDao){
            this.petDao=petDao;
        }

        @Override
        protected Void doInBackground(MyPet... myPet){
            petDao.addPet(myPet[0]);
            return null;
        }
    }
    private static class deleteAsync extends AsyncTask<MyPet, Void, Void> {
        PetDao petDao;
        public deleteAsync(PetDao petDao){
            this.petDao=petDao;
        }

        @Override
        protected Void doInBackground(MyPet... myPet){
            petDao.deletePet(myPet[0]);
            return null;
        }
    }

    private static class updateAsync extends AsyncTask<MyPet, Void, Void> {
        PetDao petDao;
        public updateAsync(PetDao petDao){
            this.petDao=petDao;
        }

        @Override
        protected Void doInBackground(MyPet... myPet){
            petDao.updatePet(myPet[0]);
            return null;
        }
    }
}
