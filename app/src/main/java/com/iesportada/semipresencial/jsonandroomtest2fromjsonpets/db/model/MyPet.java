package com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.db.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "pets")
public class MyPet {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    @NonNull
    @ColumnInfo(name = "name")
    private String name;
    @NonNull
    @ColumnInfo(name = "race")
    private String race;
    @NonNull
    @ColumnInfo(name = "age")
    private String age;
    @NonNull
    @ColumnInfo(name = "photo")
    private String photo;

    public MyPet(@NonNull String name, @NonNull String race, @NonNull String age, @NonNull String photo) {
        this.id = id;
        this.name = name;
        this.race = race;
        this.age = age;
        this.photo = photo;
    }

    public MyPet() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getRace() {
        return race;
    }

    public void setRace(@NonNull String race) {
        this.race = race;
    }

    @NonNull
    public String getAge() {
        return age;
    }

    public void setAge(@NonNull String age) {
        this.age = age;
    }

    @NonNull
    public String getPhoto() {
        return photo;
    }

    public void setPhoto(@NonNull String photo) {
        this.photo = photo;
    }
}
