package com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.db;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.db.model.MyPet;

@Database(entities = {MyPet.class}, version = 1, exportSchema = false)
public abstract class PetDatabase extends RoomDatabase {
    public abstract PetDao petListDao();

    private static final String DATABSE_NAME = "mypets";

    private static PetDatabase INSTANCE;


    public static PetDatabase getInstance(final Context context) {
        if (INSTANCE == null) {
            synchronized (PetDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(
                            context.getApplicationContext(), PetDatabase.class, DATABSE_NAME)
                            .fallbackToDestructiveMigration()
                            .addCallback(mRoomCallBack)
                            .allowMainThreadQueries()//Guarrada para permitir la edición, ya que pasando un objeto no actualiza. Explicado en Repo
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static final RoomDatabase.Callback mRoomCallBack = new RoomDatabase.Callback() {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            new PopulateDbAsync(INSTANCE).execute();
        }
    };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void>{
        private final PetDao petDao;
        PopulateDbAsync(PetDatabase db){
            petDao=db.petListDao();
        }
        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }

    }
}
