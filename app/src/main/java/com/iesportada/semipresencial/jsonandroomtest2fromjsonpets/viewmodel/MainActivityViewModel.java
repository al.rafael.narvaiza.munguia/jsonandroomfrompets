package com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.viewmodel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.db.model.MyPet;
import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.repository.Repository;

import java.util.List;

public class MainActivityViewModel extends AndroidViewModel {

    private final Repository repo;
    private LiveData<List<MyPet>> petList;

    public MainActivityViewModel(Application application){
        super(application);
        repo = new Repository(application);
        petList= repo.getPetList();
    }


    public void setPetList(){
        petList= repo.getPetList();
    }

    public LiveData<List<MyPet>> getPets(){
        return petList;
    }

    public void insert(MyPet myPet){
        repo.addPet(myPet);
        getPets();
    }
/*
    public void delete(MyPet myPet){
        repo.deletePet(myPet);
        getPets();
    }

 */


    public void update(MyPet myPet){
        repo.updatePet(myPet);
        getPets();
    }



    /*
    public void update(String nombre, String raza, String imagen, String edad, int id){
        repo.updateSelectedPet(nombre, raza, imagen, edad, id);
        getPets();
    }

     */
}
