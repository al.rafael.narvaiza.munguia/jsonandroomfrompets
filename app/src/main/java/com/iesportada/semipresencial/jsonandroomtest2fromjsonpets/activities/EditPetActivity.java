package com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.databinding.ActivityEditPetBinding;
import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.db.model.MyPet;
import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.sandbox.Utils;
import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.viewmodel.MainActivityViewModel;

import java.util.List;

public class EditPetActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private ActivityEditPetBinding binding;
    private MainActivityViewModel viewModelUpdate;
    Utils utils = new Utils();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityEditPetBinding.inflate(getLayoutInflater());
        View v = binding.getRoot();
        setContentView(v);
        initViewModel();
        getIncomingIntent();
        binding.buttonDiscard.setOnClickListener(v1->finish());
        binding.buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyPet updatedMyPet;
                String nombre,raza,imagen, edad;
                int id;
                nombre=binding.editTextTextPerroName.getText().toString();
                raza=binding.editTextTextPerroRaza.getText().toString();
                imagen=binding.editTextTextPerroImagen.getText().toString();
                edad=binding.editTextNumber.getText().toString();
                id = Integer.valueOf(binding.textViewPerroTittle.getText().toString());
                if(!Utils.inputFieldsAreChecked(nombre,raza,imagen,edad).isEmpty()){
                    showMessage(Utils.inputFieldsAreChecked(nombre,raza,imagen,edad));
                }
                else {
                    updatedMyPet = new MyPet(nombre,raza,edad,imagen);
                    viewModelUpdate.update(updatedMyPet);

                    finish();
                }

            }
        });
        viewModelUpdate.getPets();
    }

    private void getIncomingIntent() {
        String nombre,raza,edad,imagen;
        int id;
        if(getIntent().hasExtra("id")
                && getIntent().hasExtra("nombre")
                &&getIntent().hasExtra("raza")
                &&getIntent().hasExtra("edad"))
        {
            nombre=getIntent().getStringExtra("nombre");
            raza=getIntent().getStringExtra("raza");
            edad=getIntent().getStringExtra("edad");
            imagen=getIntent().getStringExtra("imagen");
            id=getIntent().getIntExtra("id", 0);

            binding.textViewPerroTittle.setText(String.valueOf(id));
            binding.editTextTextPerroName.setText(nombre);
            binding.editTextNumber.setText(edad);
            binding.editTextTextPerroRaza.setText(raza);
            binding.editTextTextPerroImagen.setText(imagen);
        }
    }

    private void initViewModel() {
        viewModelUpdate = new ViewModelProvider(this).get(MainActivityViewModel.class);
        viewModelUpdate.getPets().observe(this, new Observer<List<MyPet>>() {
            @Override
            public void onChanged(List<MyPet> myPets) {

            }
        });
    }

    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        parent.getItemAtPosition(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
}