package com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.recyclerview;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.activities.EditPetActivity;
import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.activities.MainActivity;
import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.databinding.RecyclerviewCardBinding;
import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.db.model.MyPet;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class PetListAdapter extends RecyclerView.Adapter<PetListAdapter.MyViewHolder> {

    private Context context;
    private List<MyPet> myPetList;
    private HandlePetClick clickListener;

    public PetListAdapter(Context context) {
        this.context = context;
        this.myPetList = new ArrayList<>();
    }

    public PetListAdapter(Context context, HandlePetClick clickListener) {
        this.context = context;
        this.clickListener = clickListener;
    }

    public void setPetList(List<MyPet> myPetList) {
        this.myPetList = myPetList;
        notifyDataSetChanged();
    }


    @NonNull
    @NotNull
    @Override
    public PetListAdapter.MyViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new MyViewHolder(RecyclerviewCardBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull @NotNull PetListAdapter.MyViewHolder holder, int position) {
/*
        if (context==null){
            return;
        }

 */
        if (myPetList != null) {

            MyPet myPet = myPetList.get(position);
            String message = String.valueOf(position);
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            holder.binding.textViewPetName.setText(myPet.getName());
            holder.binding.textViewPetRace.setText(myPet.getRace());
            holder.binding.textViewEdad.setText(myPet.getAge());
            Glide
                    .with(context)
                    .load(myPet.getPhoto())
                    .into(holder.binding.imageViewPet);


            holder.binding.imageViewEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    clickListener.editPet(myPetList.get(position));
                    Intent intent = new Intent(context, EditPetActivity.class);
                    intent.putExtra("id", myPet.getId());
                    intent.putExtra("nombre", myPet.getName());
                    intent.putExtra("raza", myPet.getRace());
                    intent.putExtra("imagen", myPet.getPhoto());
                    intent.putExtra("edad", myPet.getAge());
                    context.startActivity(intent);
                }
            });
        }


    }


    @Override
    public int getItemCount() {
        if (myPetList == null || myPetList.size() == 0) {
            return 0;
        } else
            return myPetList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RecyclerviewCardBinding binding;

        public MyViewHolder(@NonNull @NotNull RecyclerviewCardBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;

        }
    }

    public interface HandlePetClick {
        void editPet(MyPet myPet);

        void deletePet(MyPet myPet);
    }
}
