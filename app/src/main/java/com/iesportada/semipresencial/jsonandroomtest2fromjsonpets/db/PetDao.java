package com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.iesportada.semipresencial.jsonandroomtest2fromjsonpets.db.model.MyPet;

import java.util.List;

@Dao
public interface PetDao {
    @Transaction
    @Query("SELECT * FROM pets")
    LiveData<List<MyPet>> getAllPets();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void addPet(MyPet myPet);

    @Transaction
    @Query("SELECT * FROM pets WHERE id = :id")
    LiveData<MyPet> getUniquePet(Integer id);

    @Delete
    void deletePet(MyPet myPet);


    @Update(entity = MyPet.class)
    void updatePet(MyPet myPet);

//Forma para hacerlo con executorService

    @Query("UPDATE pets SET name = :nombre, race = :raza, photo = :imagen, age = :edad WHERE id = :id")
    void updateSelectedPet(String nombre, String raza, String imagen, String edad, int id);




}
